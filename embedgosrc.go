// Embed a file or files in a go source
//
// Basic usage...
//    embedsrcgo fileName1 argName1 fileName2 argName2 packageName
// This will print to stdout a go source file.
// It will embed the filename's data into the varname variable.
package main

import (
	"os"
	"fmt"
	"bufio"
	"strings"
)

const helpStr = `%v fileName1 argName1 fileName2 argName2 packageName
If packageName isn't given then it defaults to main.
`

const buffSize = 1024

var bufStdout *bufio.Writer

func init() {
	bufStdout = bufio.NewWriter(os.Stdout)
}

func bytesToText(bytes []byte) string {
	s := fmt.Sprintf("%v", bytes)
	s = strings.Replace(s, " ", ",", -1)
	return s[1:len(s)-1]
}

func printVarData(varName, fileName string, fout *bufio.Writer) error {
	realFin, err := os.Open(fileName)
	if err != nil {
		return err
	}
	defer realFin.Close()
	fin := bufio.NewReader(realFin)

	firstByte, _ := fin.ReadByte()
	fout.WriteString(fmt.Sprintf("var %v []byte = []byte{%v", varName, firstByte))
	buff := make([]byte, buffSize)
	for err == nil {
		var n int
		n, err = fin.Read(buff)
		if err != nil { break }
		fout.WriteString(",\n"+bytesToText(buff[:n]))
	}
	fout.WriteString("}\n")
	fout.Flush()
	return nil
}

func main() {
	if len(os.Args) <= 1 {
		fmt.Printf(helpStr, os.Args[0])
		return
	}
	pack := "main"
	if (len(os.Args) - 1) & 1 == 1 { //If there's an odd number of arguments
		pack = os.Args[len(os.Args) - 1]
	}
	fmt.Println("package", pack)
	for i := 1; i + 1 < len(os.Args); i += 2 {
		err := printVarData(os.Args[i+1], os.Args[i], bufStdout)
		if err != nil {
			panic(err)
		}
	}
}
